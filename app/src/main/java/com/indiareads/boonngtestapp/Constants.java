package com.indiareads.boonngtestapp;

public class Constants {

    public static String YEAR = "year";
    public static String MONTH = "month";
    public static String DAY = "day";
    public static String FORMAT = "format";
    public static String DATE = "date";
    public static String TIME = "time";
    public static String MAINDATE = "mainDate";
}
