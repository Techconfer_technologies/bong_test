package com.indiareads.boonngtestapp;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FactoryUtil {

    public static String[] getFormat() {

        final String[] formats = {"12 Hour Format", "24 Hours Format"};
        return formats;
    }

    public static String getCurrentDate(Calendar calendar) {
        String date = "";
        calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        date = day + "/" + getMonth(month + 1) + "/" + year;

        return date;
    }

    public static String getDefaultDate() {
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = day + "/" + FactoryUtil.getMonth(month + 1) + "/" + year;

        return date;
    }

    public static String getRelaventTime(String relavantTime) {
        String relavantStr = relavantTime;
        if (relavantTime.contains("AM") || relavantTime.contains("PM")) {

            relavantTime.replace(" AM", "");
            relavantTime.replace(" PM", "");

            String[] time = relavantTime.split(":");
            int hour = Integer.valueOf(time[0]);

            if (relavantStr.contains("PM")) {
                hour = hour + 12;
                relavantStr = hour + ":" + "00";
            } else {
                if (hour < 10) {
                    relavantStr = "0" + hour + ":" + "00";
                } else {
                    relavantStr = hour + ":" + "00";
                }
            }

        } else {

            String[] time = relavantTime.split(":");
            int hour = Integer.valueOf(time[0]);

            if (hour > 12) {
                hour = hour - 12;
                if (hour < 10) {
                    relavantStr = "0" + hour + ":" + "00 PM";
                } else {
                    relavantStr = hour + ":" + "00 PM";
                }

            } else {
                relavantStr = "0" + hour + ":" + "00 AM";
            }
        }

        return relavantStr;
    }

    public static List<CalenderModel> getList(String format, int year, int month, int day) {
        List<CalenderModel> list;

        if (format.equalsIgnoreCase("12")) {
            if (isWeekend(year, month, day)) {
                list = get12HourList(true);
            } else {
                list = get12HourList(false);
            }
        } else {
            if (isWeekend(year, month, day)) {
                list = get24HourList(true);
            } else {
                list = get24HourList(false);
            }
        }
        return list;
    }

    public static void showDialog(Context context, String date, String time) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View view2 = layoutInflaterAndroid.inflate(R.layout.alert_dialog, null);
        builder.setView(view2);
        builder.setCancelable(true);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        TextView timeSlot = alertDialog.findViewById(R.id.time);
        TextView dateSlot = alertDialog.findViewById(R.id.date);
        Button btnConfirm = alertDialog.findViewById(R.id.btnConfirm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });

        timeSlot.setText(time);
        dateSlot.setText(date);
    }

    public static void updateTime(String date, TextView textView) {
        String first = "Available time for : ";
        String next = "<font color='#008577'>" + date + "</font>";
        textView.setText(Html.fromHtml(first + next));
    }

    public static long convertDateToMilliseconds(int year, int month, int day) {
        long dateValue = 0;

        try {

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            dateValue = calendar.getTimeInMillis();

        } catch (Exception e) {

        }

        return dateValue;
    }

    public static boolean isWeekend(int year, int month, int selectedDay) {

        boolean isWeekend = false;

        Calendar startDate = Calendar.getInstance();

        startDate.set(year, month, selectedDay);
        if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
                || startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            isWeekend = true;
        } else {
            isWeekend = false;
        }

        return isWeekend;
    }

    public static ArrayList<CalenderModel> get24HourList(boolean isWeekend) {
        ArrayList<CalenderModel> slots = new ArrayList<>();

        for (int i = 1; i <= 24; i++) {

            CalenderModel calenderModel = new CalenderModel();

            if (i < 10) {
                calenderModel.setTime("0" + i + ":00");
            } else {
                calenderModel.setTime(i + ":00");
            }

            if (isWeekend) {
                if (i >= 8 && i <= 22)
                    calenderModel.setClickable(true);
                else
                    calenderModel.setClickable(false);
            } else {
                if (i >= 9 && i <= 22)
                    calenderModel.setClickable(true);
                else
                    calenderModel.setClickable(false);
            }

            slots.add(calenderModel);
        }
        return slots;
    }

    public static ArrayList<CalenderModel> get12HourList(boolean isWeekend) {
        ArrayList<CalenderModel> slots = new ArrayList<>();

        for (int i = 1; i <= 24; i++) {


            if (i <= 12) {

                if (i < 10) {

                    CalenderModel calenderModel = new CalenderModel();
                    calenderModel.setTime("0" + i + ":00 AM");
                    if (isWeekend) {
                        if (i >= 1 && i <= 7)
                            calenderModel.setClickable(false);
                        else
                            calenderModel.setClickable(true);
                    } else {
                        if (i >= 1 && i <= 8)
                            calenderModel.setClickable(false);
                        else
                            calenderModel.setClickable(true);
                    }

                    slots.add(calenderModel);

                } else {
                    CalenderModel calenderModel = new CalenderModel();
                    calenderModel.setTime(i + ":00 AM");
                    calenderModel.setClickable(true);
                    slots.add(calenderModel);
                }

            } else {
                int j = i;

                j = j - 12;

                if (j < 10) {

                    CalenderModel calenderModel = new CalenderModel();
                    calenderModel.setTime("0" + j + ":00 PM");
                    calenderModel.setClickable(true);
                    slots.add(calenderModel);

                } else {

                    CalenderModel calenderModel = new CalenderModel();
                    calenderModel.setTime(j + ":00 PM");
                    if (j == 10) {
                        calenderModel.setClickable(true);
                    } else {
                        calenderModel.setClickable(false);
                    }
                    slots.add(calenderModel);
                }
            }
        }

        return slots;
    }

    public static String getMonth(int month) {
        String monthSelected = "";

        if (month == 1) {
            monthSelected = "Jan";
        } else if (month == 2) {
            monthSelected = "Feb";
        } else if (month == 3) {
            monthSelected = "March";
        } else if (month == 4) {
            monthSelected = "April";
        } else if (month == 5) {
            monthSelected = "May";
        } else if (month == 6) {
            monthSelected = "June";
        } else if (month == 7) {
            monthSelected = "July";
        } else if (month == 8) {
            monthSelected = "Aug";
        } else if (month == 9) {
            monthSelected = "Sept";
        } else if (month == 10) {
            monthSelected = "Oct";
        } else if (month == 11) {
            monthSelected = "Nov";
        } else if (month == 12) {
            monthSelected = "Dec";
        }

        return monthSelected;
    }
}
