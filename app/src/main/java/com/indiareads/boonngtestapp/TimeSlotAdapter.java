package com.indiareads.boonngtestapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.ViewHolder> {

    private Context mContext;
    private String selectedTime = "";
    private List<CalenderModel> mData;
    saveSlotTime slotTime;
    String relaventStr = "";

    // data is passed into the constructor
    public TimeSlotAdapter(Context context, List<CalenderModel> data, String selectedDate, saveSlotTime slotTime) {
        this.mData = data;
        this.slotTime = slotTime;
        this.mContext = context;
        this.selectedTime = selectedDate;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeslot_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final CalenderModel calenderModel = mData.get(position);
        String item = mData.get(position).getTime();
        holder.myTextView.setText(item);


        if (mData.get(position).isClickable()) {
            holder.myTextView.setTextColor(mContext.getResources().getColor(R.color.black));
        } else {
            holder.myTextView.setTextColor(mContext.getResources().getColor(R.color.grey));
        }

        if (!selectedTime.equalsIgnoreCase("")) {
            relaventStr = FactoryUtil.getRelaventTime(selectedTime);
        }

        if (selectedTime.equalsIgnoreCase(calenderModel.getTime()) || relaventStr.equalsIgnoreCase(calenderModel.getTime())) {
            holder.myTextView.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.cardViewMain.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.cardViewMain.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.get(position).isClickable()) {
                    selectedTime = mData.get(position).getTime();
                    notifyDataSetChanged();
                    slotTime.selectedSlot(selectedTime);
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        CardView cardViewMain;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.timeText);
            cardViewMain = itemView.findViewById(R.id.cardViewMain);
        }
    }
}


