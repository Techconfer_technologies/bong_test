package com.indiareads.boonngtestapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView timeSlotView;
    TextView timeAvailable;
    CalendarView calenderView;
    Calendar calendar;
    TimeSlotAdapter timeSlotAdapter;
    Spinner spinner;

    String format = "12";
    long dateValue;
    String date = "";
    int currentYear, currentMonth, currentDayMonth;
    String selectedDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calenderView = findViewById(R.id.calenderView);
        spinner = findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                R.layout.spinner_text, FactoryUtil.getFormat());

        adapter.setDropDownViewResource(R.layout.spinner_text);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    format = "12";

                    if (FactoryUtil.isWeekend(currentYear, currentMonth, currentDayMonth)) {
                        List<CalenderModel> list = new ArrayList<>();

                        list = FactoryUtil.get12HourList(true);

                        setAdapter(list);

                    } else {

                        List<CalenderModel> list = new ArrayList<>();

                        list = FactoryUtil.get12HourList(false);
                        setAdapter(list);
                    }
                } else {

                    format = "24";

                    if (FactoryUtil.isWeekend(currentYear, currentMonth, currentDayMonth)) {
                        List<CalenderModel> list = new ArrayList<>();

                        list = FactoryUtil.get24HourList(true);

                        setAdapter(list);
                    } else {

                        List<CalenderModel> list = new ArrayList<>();

                        list = FactoryUtil.get24HourList(false);

                        setAdapter(list);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        timeAvailable = findViewById(R.id.timeAvailable);

        timeSlotView = findViewById(R.id.timeSlotView);

        calendar = Calendar.getInstance();

        String currentDate = FactoryUtil.getCurrentDate(calendar);

        FactoryUtil.updateTime(currentDate, timeAvailable);//updateTime

        timeSlotView.setLayoutManager(new GridLayoutManager(this, 4));

        calenderView.setDate(calendar.getTimeInMillis());

        dateValue = calendar.getTimeInMillis();

        calenderView.setMinDate(calendar.getTimeInMillis());

        date = FactoryUtil.getDefaultDate();

        calenderView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {

                currentYear = year;
                currentMonth = month;
                currentDayMonth = day;

                selectedDate = "";
                setAdapter(FactoryUtil.getList(format, currentYear, currentMonth, currentDayMonth));

                date = day + "/" + FactoryUtil.getMonth(month + 1) + "/" + year;
                FactoryUtil.updateTime(date, timeAvailable);

                if (FactoryUtil.isWeekend(year, month, day)) {
                    List<CalenderModel> list = new ArrayList<>();

                    if (format.equalsIgnoreCase("12")) {
                        list = FactoryUtil.get12HourList(true);
                    } else {
                        list = FactoryUtil.get24HourList(true);
                    }

                    setAdapter(list);
                } else {

                    List<CalenderModel> list = new ArrayList<>();

                    if (format.equalsIgnoreCase("12")) {
                        list = FactoryUtil.get12HourList(false);
                    } else {
                        list = FactoryUtil.get24HourList(false);
                    }

                    setAdapter(list);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        dateValue = FactoryUtil.convertDateToMilliseconds(currentYear, currentMonth, currentDayMonth);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dateValue = FactoryUtil.convertDateToMilliseconds(currentYear, currentMonth, currentDayMonth);
    }

    public void setAdapter(List<CalenderModel> list) {
        timeSlotAdapter = new TimeSlotAdapter(MainActivity.this, list, selectedDate, new saveSlotTime() {
            @Override
            public void selectedSlot(String selectedTime) {
                selectedDate = selectedTime;
                String time = "Selected Slot : " + selectedTime;
                String dateS = "Selected Date : " + date;
                FactoryUtil.showDialog(MainActivity.this, dateS, time);
            }
        });
        timeSlotView.setAdapter(timeSlotAdapter);
    }

    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);

        icicle.putInt(Constants.YEAR, currentYear);
        icicle.putInt(Constants.MONTH, currentMonth);
        icicle.putInt(Constants.DAY, currentDayMonth);
        icicle.putString(Constants.FORMAT, format);
        icicle.putString(Constants.DATE, date);
        icicle.putString(Constants.TIME, selectedDate);
        icicle.putLong(Constants.MAINDATE, FactoryUtil.convertDateToMilliseconds(currentYear, currentMonth, currentDayMonth));

    }

    @Override
    protected void onRestoreInstanceState(Bundle icicle) {
        super.onRestoreInstanceState(icicle);

        if (icicle != null) {

            currentYear = icicle.getInt(Constants.YEAR);//, currentYear);
            currentDayMonth = icicle.getInt(Constants.MONTH);//, currentMonth);
            currentDayMonth = icicle.getInt(Constants.DAY);//, currentDayMonth);
            format = icicle.getString(Constants.FORMAT);//, format);
            selectedDate = icicle.getString(Constants.TIME);
            dateValue = icicle.getLong(Constants.MAINDATE);
            date = icicle.getString(Constants.DATE);

            FactoryUtil.updateTime(date, timeAvailable);

            calenderView.setDate(dateValue);

            setAdapter(FactoryUtil.getList(format, currentYear, currentMonth, currentDayMonth));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            calenderView.setDate(dateValue);
        } else {
            calenderView.setDate(dateValue);
        }
    }
}
